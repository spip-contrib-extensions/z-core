<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zpip?lang_cible=pl
// ** ne pas modifier le fichier **

return [

	// A
	'accueil' => 'Strona główna',
	'adapte_de' => 'oparty na',

	// C
	'commentaire' => 'komentarz',
	'commentaires' => 'komentarze',
	'conception_graphique_par' => 'Projekt graficzny ©',

	// D
	'date_forum' => '@date@ o @heure@',

	// I
	'info_1_commentaire' => 'komentarz 1',
	'info_nb_commentaires' => 'komentarze @nb@',

	// L
	'lire_la_suite' => 'Przeczytaj więcej',
	'lire_la_suite_de' => ' od ',

	// P
	'pagination_next' => 'Następny »',
	'pagination_pages' => 'Strony',
	'pagination_prev' => '« Poprzedni',
	'personaliser_nav' => 'Spersonalizuj menu',

	// S
	'sous_licence' => 'na podstawie licencji',

	// Z
	'zapl_loading' => 'W trakcie ładowania...',
	'zapl_reload_off' => 'Kliknij tutaj jeśli strona pozostaje niekompletna (lub aktywuj javascript w przeglądarce)',
];

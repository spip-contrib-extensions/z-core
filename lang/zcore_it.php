<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zpip?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'accueil' => 'Accoglienza',
	'adapte_de' => 'adattato da',

	// C
	'commentaire' => 'commento',
	'commentaires' => 'commenti',
	'conception_graphique_par' => 'Vestizione visiva ©',

	// D
	'date_forum' => 'Il @date@ alle @heure@',

	// I
	'info_1_commentaire' => '1 commento',
	'info_nb_commentaires' => '@nb@ commenti',

	// L
	'lire_la_suite' => 'Per saperne di più',
	'lire_la_suite_de' => ' di ',

	// P
	'pagination_next' => 'Seguente »',
	'pagination_pages' => 'Pagine',
	'pagination_prev' => '« Precedente',
	'personaliser_nav' => 'Personalizza il menù',

	// S
	'sous_licence' => 'con Licenza',

	// Z
	'zapl_loading' => 'Caricamento in corso...',
	'zapl_reload_off' => 'Clicca qui se la pagina rimane incompleta (o attiva javascript nel tuo browser)',
];
